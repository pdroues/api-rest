<?php

/** @file publication.php
 * this file contains all the function related to the publication
 */

require_once("../utils/Database.php");
require_once("../utils/Exception.php");

require_once("../functions/reaction.php");

/**
 * @brief get all the information about a publication, used by other function
 * @param int $id_publication the identifier of the ppublication
 * @return array
 * @throws ExceptionDatabase
 */
function get_publication(int $id_publication) : array
{
	$sqlrequest = 'SELECT * FROM `publication` WHERE `id_publication` = :id_publication';
	$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
	$request->bindParam('id_publication', $id_publication, PDO::PARAM_INT);
	if($request->execute() === false) { throw new ExceptionDatabase(); }
	$result = $request->fetch(PDO::FETCH_ASSOC);

	$pseudo = get_pseudo_member($result['id_member']);
	$reaction = get_reaction_of_publication_moderator($result['id_publication']);

	return array('id_publication' => $result['id_publication'], 'content' => $result['content'], 'writer' => $pseudo, 'date_publication' => $result['date_publication'], 'nb_like' => $reaction['like_count'], 'nb_dislike' => $reaction['dislike_count'], 'like' => $reaction['like'], 'dislike' => $reaction['dislike']);
}

/**
 * @brief return a publication for a moderator
 * @param int $id_publication the identifier of the publication
 * @return array 
 */
function get_publication_moderator(int $id_publication) : array
{
	return get_publication($id_publication);
}

/**
 * @brief return a publication for a publisher
 * @param int $id_publication the identifier of the publication
 * @return array 
 */
function get_publication_publisher(int $id_publication) : array
{
	$publication = get_publication($id_publication);
	array_splice($publication, -2);
	return $publication;
}

/**
 * @brief return a publication for a visitor
 * @param int $id_publication the identifier of the publication
 * @return array 
 */
function get_publication_visitor(int $id_publication) : array
{
	$publication = get_publication($id_publication);
	array_splice($publication, -4);
	return $publication;
}

/**
 * @brief get all the information about all the publications, make good names, used by other function
 * @return array
 * @throws ExceptionDatabase
 */
function get_all_publication() : array
{
	$sqlrequest = 'SELECT * FROM `publication`';
	$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
	if($request->execute() === false) { throw new ExceptionDatabase(); }
	$result = $request->fetchAll(PDO::FETCH_ASSOC);

	$publications = array();

	foreach($result as $publication) {
		$pseudo = get_pseudo_member($publication['id_member']);
		$reaction = get_reaction_of_publication_moderator($publication['id_publication']);
		$publication = array('id_publication' => $publication['id_publication'], 'content' => $publication['content'], 'writer' => $pseudo, 'date_publication' => $publication['date_publication'], 'nb_like' => $reaction['like_count'], 'nb_dislike' => $reaction['dislike_count'], 'like' => $reaction['like'], 'dislike' => $reaction['dislike']);
		array_push($publications, $publication);
	}
	return $publications;
}

/**
 * @brief return all the publications for a moderator
 * @return array 
 */
function get_all_publication_moderator() : array
{
	return get_all_publication();
}

/**
 * @brief return all the publications for a publisher
 * @return array 
 */
function get_all_publication_publisher() : array
{
	$publications = get_all_publication();
	$publication_for_publisher = array();
	foreach($publications as $publication) {
		array_splice($publication, -2);
		array_push($publication_for_publisher, $publication);
	}
	return $publication_for_publisher;
}

/**
 * @brief return all the publications for a moderator
 * @param string $pseudo the pseudo of the member (serve as identifier)
 * @return array 
 */
function get_all_self_publication(string $pseudo) : array
{
	$publications = get_all_publication_publisher();
	$self_publication = array();
	foreach($publications as $publication) {
		if($publication['writer'] == $pseudo) {
			array_splice($publication, -2);
			array_push($self_publication, $publication);
		}
	}
	return $self_publication;
}

/**
 * @brief return all the publications for a visitor
 * @return array 
 */
function get_all_publication_visitor() : array
{
	$publications = get_all_publication();
	$publication_for_visitor = array();
	foreach($publications as $publication) {
		array_splice($publication, -4);
		array_push($publication_for_visitor, $publication);
	}
	return $publication_for_visitor;
}

/**
 * @brief permit at a publisher to create a publication
 * @param string $content the body/data of the publication
 * @param int $id_member the identifier of the member who publish this
 * @return array 
 * @throws ExceptionDatabase
 */
function create_publication(string $content, int $id_member) : array
{	
	$date = date("Y-m-d H:i:s");
	$sqlrequest = 'INSERT INTO publication (`content`,`id_member`,`date_publication`) VALUES(:content,:id_member,:date_publication)';
	$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
	$request->bindparam('id_member', $id_member, PDO::PARAM_INT);
	$request->bindparam('content', $content, PDO::PARAM_STR);
	$request->bindparam('date_publication', $date, PDO::PARAM_STR);
	if($request->execute() === false ) { throw new ExceptionDatabase(); }
	return get_publication_publisher(Database::get_instance()->get_connection()->lastInsertId());
}

/**
 * @brief permit at a publisher to modify is own publication
 * @param int $id_publication the identifier of the publication
 * @param string $content the body/data of the publication who comes in remplacement of older
 * @return array 
 * @throws ExceptionDatabase
 */
function modify_publication(int $id_publication, string $content) : array
{
	$sqlrequest = 'UPDATE `publication` SET `content` = :content WHERE `id_publication` = :id_publication' ;
	$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
	$request->bindparam("id_publication", $id_publication, PDO::PARAM_INT);
	$request->bindparam("content", $content, PDO::PARAM_STR);
	if($request->execute() === false) { throw new ExceptionDatabase(); }
	return get_publication_publisher($id_publication);
}

/**
 * @brief permit at a publisher to deleto is own publication, or a moderator
 * @param int $id_publication the identifier of the publication
 * @return void 
 * @throws ExceptionDatabase
 */
function delete_publication(int $id_publication) : void
{
	$sqlrequest = 'DELETE FROM `publication` WHERE `id_publication`= :id_publication';
	$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
	$request->bindParam("id_publication", $id_publication, PDO::PARAM_INT);
	if($request->execute() === false) { throw new ExceptionDatabase(); }
}

/**
 * @brief check if the publication exist
 * @param int $id_publication the identifier of the publication
 * @return bool true if exist, false if not
 * @throws ExceptionDatabase
 */
function exist_publication(int $id_publication) : bool
{
	$sqlrequest = 'SELECT * FROM `publication` WHERE `id_publication` = :id_publication';
	$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
	$request->bindParam('id_publication', $id_publication, PDO::PARAM_INT);
	if($request->execute() === false) { throw new ExceptionDatabase(); }
	if($request->rowCount() > 0) { return true; } else { return false; }
}
