<?php

/** @file utils.php
 * this file contains some functions
 */

/**
 * @brief return the payload of the JWT token under a php array
 * @param string $bearer_token	JWT token of the user
 * @return array php array of the jwt body's token
 */
function get_body_token(string $bearer_token) : array
{
	$tokenParts = explode('.', $bearer_token);
	$payload = base64_decode($tokenParts[1]);
	return (array) json_decode($payload);
}


/**
 * @brief retun the jwt secret
 * this function exist for increase the modularity, if we stop using environemnt variable , i will be esayest to switch
 * @return string the jwt secret
 */
function get_jwt_secret() : string
{
	return getenv('JWT_SECRET');
}

/**
 * @brief send the response to the client
 * @param mixed $status HTTP status code
 * @param mixed $status_message description of the status
 * @param mixed $data formated json data send, null if an error oucured
 */
function deliver_response($status, $status_message, $data)
{
	header("HTTP/1.1 $status $status_message");
	$response['status'] = $status;
	$response['status_message'] = $status_message;
	$response['data'] = $data;
	$json_response = json_encode($response);
	echo $json_response;
}
