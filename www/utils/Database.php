<?php

/** @file Database.php
 * this file contains the Database class
 */

/**
 * @class Database
 * @brief this class permit to interact with the database
 * this class is an implementation of the singleton design pattern
 */
class Database
{
	private static $_instance;	/**< instance of Databbase */
	private $_connection;	/**< the pdo object used to make request*/

	/**
	 * @brief constructor of Database class
	 * create the pdo object for the connection
	 * use environement variables for get connection information
	 */
	private function __construct()
	{

		$SGBD = getenv('SGBD');
		$HOST = getenv('HOST_DB');
		$DBNAME = getenv('NAME_DB');

		$DBUSER = getenv('USER_DB');
		$DBPASS = getenv('USER_PASS');

		$DSN = $SGBD.":host=".$HOST.";dbname=".$DBNAME.";charset=utf8mb4";

		$options = [
			PDO::ATTR_EMULATE_PREPARES   => false,
			PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
		];

		$this->_connection = new PDO($DSN, $DBUSER, $DBPASS, $options);
	}

	/**
	 * @brief returns the instance of the class, if it does not exist it will be created
	 * @return Database a database object for execute the get_connection() on it
	 */
	public static function get_instance() : Database
	{
		if (!self::$_instance) {
			self::$_instance = new Database();
		}
		return self::$_instance;
	}

	/**
	 * @brief return the PDO object
	 * @return PDO  PDO object used for prepare request
	 */
	public function get_connection() : PDO
	{
		return $this->_connection;
	}

}
