<?php

/** @file reaction.php
 * this file contains all the function related to the reaction
 */

require_once("../utils/Database.php");
require_once("../utils/Exception.php");

/**
 * @brief return the reaction of a publication for a moderator
 * @param int $id_publication the identifier of the publication
 * @return array
 * @throws ExceptionDatabase
 */
function get_reaction_of_publication_moderator(int $id_publication) : array
{
	$sqlrequest = 'SELECT `type`, `id_member` FROM `reaction` WHERE `id_publication` = :id_publication';
	$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
	$request->bindParam('id_publication', $id_publication);
	if($request->execute() === false) { throw new ExceptionDatabase(); }
	$result = $request->fetchAll(PDO::FETCH_ASSOC);
	$like = array();
	$dislike = array();
	foreach($result as $r) {
		$pseudo = get_pseudo_member($r['id_member']);
		if($r['type'] === true) {
			array_push($like, $pseudo);
		} else {
			array_push($dislike, $pseudo);
		}
	}
	$like_count = count_like($id_publication);
	$dislike_count = count_dislike($id_publication);
	return array('like' => $like,'like_count' => $like_count,'dislike' => $dislike,'dislike_count' => $dislike_count);
}

 /**
 * @brief return the reaction of a publication for a publisher
 * @param int $id_publication the identifier of the publication
 * @return array 
 */
function get_reaction_of_publication_publisher(int $id_publication) : array
{
	$like_count = count_like($id_publication);
	$dislike_count = count_dislike($id_publication);
	return array('like_count' => $like_count,'dislike_count' => $dislike_count);
}

/**
 * @brief return the number of like of a publication 
 * @param int $id_publication the identifier of the publication
 * @return int number of like of the publication
 * @throws ExceptionDatabase
 */
function count_like(int $id_publication) : int
{
	$sqlrequest = 'SELECT count(*) FROM `reaction` WHERE `id_publication` = :id_publication AND `type` = true';
	$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
	$request->bindParam('id_publication', $id_publication);
	if($request->execute() === false) { throw new ExceptionDatabase(); }
	return $request->fetch(PDO::FETCH_COLUMN);
}

/**
 * @brief return the number of dislike of a publication 
 * @param int $id_publication the identifier of the publication
 * @return int number of dislike of the publication
 * @throws ExceptionDatabase
 */
function count_dislike(int $id_publication) : int
{
	$sqlrequest = 'SELECT count(*) FROM `reaction` WHERE `id_publication` = :id_publication AND `type` = false';
	$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
	$request->bindParam('id_publication', $id_publication);
	if($request->execute() === false) { throw new ExceptionDatabase(); }
	return $request->fetch(PDO::FETCH_COLUMN);}

/**
 * @brief permit to a member (who is a publisher) to like a publication
 * @param int $id_publication the identifier of the publication
 * @param int $id_member the identifier of the member
 * @return void
 * @throws ExceptionDatabase
 */
function like_publication(int $id_publication, int $id_member) : void
{
	$sqlrequest = 'INSERT INTO reaction(`id_publication`, `id_member`, `type`) VALUES(:id_publication, :id_member, 1)';
	$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
	$request->bindParam('id_publication', $id_publication);
	$request->bindParam('id_member', $id_member);
	if($request->execute() === false) { throw new ExceptionDatabase(); }
}

/**
 * @brief permit to a member (who is a publisher) to dislike a publication
 * @param int $id_publication the identifier of the publication
 * @param int $id_member the identifier of the member
 * @return void
 * @throws ExceptionDatabase
 */
function dislike_publication(int $id_publication, int $id_member) : void
{
	$sqlrequest = 'INSERT INTO reaction(`id_publication`, `id_member`, `type`) VALUES(:id_publication, :id_member, 0)';
	$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
	$request->bindParam('id_publication', $id_publication);
	$request->bindParam('id_member', $id_member);
	if($request->execute() === false) { throw new ExceptionDatabase(); }
}

/**
 * @brief remove the like or dislike of a member to the publication
 * @param int $id_publication the identifier of the publication
 * @param int $id_member the identifier of the member
 * @return void
 * @throws ExceptionDatabase
 */
function remove_reaction_of_publication(int $id_publication, int $id_member) : void
{
	$sqlrequest = 'DELETE FROM `reaction` WHERE `id_publication` = :id_publication AND `id_member` = :id_member';
	$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
	$request->bindParam('id_publication', $id_publication);
	$request->bindParam('id_member', $id_member);
	if($request->execute() === false) { throw new ExceptionDatabase(); }
}

/**
 * @brief verify if an user already interact with a publication
 * @param int $id_publication the identifier of the publication
 * @param int $id_member the identifier of the member
 * @return bool true if user already interact, false if not
 * @throws ExceptionDatabase
 */
function has_react(int $id_publication, int $id_member) : bool
{
	$sqlrequest = 'SELECT * FROM `reaction` WHERE `id_publication` = :id_publication AND `id_member`= :id_member';
	$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
	$request->bindParam('id_publication', $id_publication);
	$request->bindParam('id_member', $id_member);
	if($request->execute() === false) { throw new ExceptionDatabase(); }
	if($request->rowCount() > 0) { return true; } else { return false; }
}
