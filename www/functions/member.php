<?php

/** @file member.php
 * this file contains all the function related to the user
 */

require_once("../utils/Database.php");
require_once("../utils/Exception.php");

/**
 * @brief check if the member is a moderator
 * @param int $id_member the identifier of the member
 * @return bool true if exist, false if not
 */
function is_moderator(array $member) : bool
{
	if ($member['role'] === "moderator") { return true; } else { return false; }
}

/**
 * @brief check if the member is a publisher
 * @param int $id_member the identifier of the member
 * @return bool true if exist, false if not
 */
function is_publisher(array $member) : bool
{
	if ($member['role'] === "publisher") { return true; } else { return false; }
}

/**
 * @brief check if the member is connected
 * @param int $id_member the identifier of the member
 * @return bool true if exist, false if not
 */
function is_connected(array $member) : bool
{
	if ($member['id_member'] != -1) { return true;} else { return false; }
}

/**
 * @brief check if the member is the owner/creator of the publication
 * @param int $id_member the identifier of the member
 * @param int $id_publication the identifier of the publication
 * @return bool true if exist, false if not
 * @throws ExceptionDatabase
 */
function is_owner_of_publication(int $id_member, int $id_publication) : bool
{
	$sqlrequest = 'SELECT * FROM `publication` WHERE `id_member` = :id_member AND `id_publication` = :id_publication';
	$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
	$request->bindparam('id_member', $id_member);
	$request->bindparam('id_publication', $id_publication);
	if ($request->execute() === false) { throw new ExceptionDatabase(); }
	if ($request->rowCount() === 1) { return true; } else { return false; }
}

/**
 * @brief check the credential of the men, and return the future jwt body
 * @param string $pseudo the pseudo of the member
 * @param string $password the password (in clear text)
 * @return array the body for a future jwt token
 * @throws ExceptionInvalidCredential
 */
function connect(string $pseudo, string $password) : array
{
	$member = get_member_from_pseudo($pseudo);

	if (!password_verify($password, $member['password'])) { throw new ExceptionInvalidCredential(); }

	return array("id_member" => $member['id_member'], "pseudo" => $member['pseudo'], "role" => $member['role']);
}

/**
 * @brief return the pseudo of a member from is id
 * @param int $id_member the identifier of the member
 * @return string the member's pseudo
 * @throws ExceptionDatabase
 */
function get_pseudo_member($id_member) : string
{
	$sqlrequest = 'SELECT `pseudo` FROM `member` WHERE `id_member` = :id_member';
	$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
	$request->bindParam("id_member", $id_member, PDO::PARAM_INT);
	if ($request->execute() === false) { throw new ExceptionDatabase(); }
	return $request->fetch(PDO::FETCH_COLUMN);
}

/**
 * @brief return all the info about the member
 * @param string $pseudo the psedo of member (can be used to identify it)
 * @return array
 * @throws ExceptionDatabase
 */
function get_member_from_pseudo(string $pseudo) : array
{
	$sqlrequest = 'SELECT * FROM `member` WHERE `pseudo` = :pseudo';
	$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
	$request->bindparam('pseudo', $pseudo);
	if ($request->execute() === false) { throw new ExceptionDatabase(); }
	return $request->fetch(PDO::FETCH_ASSOC);
}

/**
 * @brief check if the member exist
 * @param int $id_member the identifier of the member
 * @return bool true if exist, false if not
 * @throws ExceptionDatabase
 */
function exist_member_id(int $id_member) : bool
{
	$sqlrequest = 'SELECT * FROM `member` WHERE `id_member` =: `id_member`';
	$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
	$request->bindparam('id_member', $id_member, PDO::PARAM_STR);
	if ($request->execute() === false) { throw new ExceptionDatabase(); }
	if ($request->rowCount() > 0) { return true; } else { return false; }
}

/**
 * @brief check if the member exist
 * @param int $id_member the identifier of the member
 * @return bool true if exist, false if not
 * @throws ExceptionDatabase
 */
function exist_member_pseudo(string $pseudo) : bool
{
	$sqlrequest = 'SELECT * FROM `member` WHERE `pseudo` =: `pseudo`';
	$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
	$request->bindparam('pseudo', $pseudo, PDO::PARAM_STR);
	if ($request->execute() === false) { throw new ExceptionDatabase(); }
	if ($request->rowCount() > 0) { return true; } else { return false; }
}
