<?php

/** @file publications.php
 * this file is the part of the rest api witch manage the publication ressouce
 */

require_once("../utils/jwt_utils.php");
require_once("../utils/Exception.php");
require_once("../utils/utils.php");

require_once("../functions/member.php");
require_once("../functions/publication.php");


header("Content-Type:application/json");
$http_method = $_SERVER['REQUEST_METHOD'];

try {
	if (!empty($_GET['id_publication'])) {
		if (!exist_publication($_GET['id_publication'])) { throw new ExceptionRessourceNotFound(); }
		$id_publication = trim(htmlentities($_GET['id_publication']));
	} else { $id_publication = -1; }

	$bearer_token = get_bearer_token();
	if ($bearer_token != null) {
		if (!is_jwt_valid($bearer_token, get_jwt_secret())) { throw new ExceptionInvalidJWTToken(); }
		$member = get_body_token($bearer_token);
	} else { $member = array("id_member" => -1, "role" => array("visitor")); }

} catch (\Throwable $th) {
	$RETURN_CODE = $th->getCode();
	$STATUS_MESSAGE = $th->getMessage();
	deliver_response($RETURN_CODE, $STATUS_MESSAGE, null);
	die();
}

switch ($http_method) {

	case "GET" :
		try {
			if (exist_publication($id_publication)) {

				if (is_moderator($member)) {
					$result = get_publication_moderator($id_publication);
				} elseif (is_publisher($member)) {
					$result = get_publication_publisher($id_publication);
				} else {
					$result = get_publication_visitor($id_publication);
				}
			} else {

				if (!empty($_GET['creator']) && $_GET['creator'] === "me") {
					if (is_moderator($member)) { throw new ExceptionInsufficiantPermission(); }
					if (!is_connected($member)) { throw new ExceptionAccountNeeded(); }
					$result = get_all_self_publication($member['pseudo']);
				} else {
					if (is_moderator($member)) {
						$result = get_all_publication_moderator();
					} elseif (is_publisher($member)) {
						$result = get_all_publication();
					} else {
						$result = get_all_publication_visitor();
					}
				}
			}

			$DATA = $result;
			$RETURN_CODE = 200;
			$STATUS_MESSAGE = "Successfully obtained";
		} catch (\Throwable $th) {
			$RETURN_CODE = $th->getCode();
			$STATUS_MESSAGE = $th->getMessage();
			$DATA = null;
		} finally {
			deliver_response($RETURN_CODE, $STATUS_MESSAGE, $DATA);
		}
		break;

	case "POST" :
		try {
			$postedData = file_get_contents('php://input');
			$data=json_decode($postedData, true);

			if (!is_connected($member)) { throw new ExceptionAccountNeeded(); }
			if (!is_publisher($member)) { throw new ExceptionInsufficiantPermission(); }
			if (empty($data['content'])) { throw new ExceptionMissingParameter(); }
			$content = trim(htmlentities($data['content']));

			$result = create_publication($content, $member['id_member']);

			$DATA = $result;
			$RETURN_CODE = 201;
			$STATUS_MESSAGE = "Successfully inserted";
		} catch (\Throwable $th) {
			$RETURN_CODE = $th->getCode();
			$STATUS_MESSAGE = $th->getMessage();
			$DATA = null;
		} finally {
			deliver_response($RETURN_CODE, $STATUS_MESSAGE, $DATA);
		}
		break;
	
	case "PUT" :
		try {
			$postedData = file_get_contents('php://input');
			$data=json_decode($postedData, true);

			if (!is_connected($member)) { throw new ExceptionAccountNeeded(); }
			if (empty($id_publication)) { throw new ExceptionMissingParameter(); }
			if (!is_publisher($member)) { throw new ExceptionInsufficiantPermission(); }
			if (!is_owner_of_publication($member['id_member'], $id_publication)) { throw new ExceptionInsufficiantPermission(); }
			if (empty($data['content'])) { throw new ExceptionMissingParameter(); }
			$content = trim(htmlentities($data['content']));

			$result = modify_publication($id_publication, $content);

			$DATA = $result;
			$RETURN_CODE = 200;
			$STATUS_MESSAGE = "Successfully updated";
		} catch (\Throwable $th) {
			$RETURN_CODE = $th->getCode();
			$STATUS_MESSAGE = $th->getMessage();
			$DATA = null;
		} finally {
			deliver_response($RETURN_CODE, $STATUS_MESSAGE, $DATA);
		}
		break;

	case "DELETE" :
		try {
			if (!is_connected($member)) { throw new ExceptionAccountNeeded(); }
			if (empty($id_publication)) { throw new ExceptionMissingParameter(); }
			if (!is_moderator($member) xor is_publisher($member)) { throw new ExceptionInsufficiantPermission(); }
			if (is_publisher($member) && !is_owner_of_publication($member['id_member'], $id_publication)) { throw new ExceptionInsufficiantPermission(); }

			delete_publication($id_publication);

			$RETURN_CODE = 200;
			$STATUS_MESSAGE = "Successfully deleted";
		} catch (\Throwable $th) {
			$RETURN_CODE = $th->getCode();
			$STATUS_MESSAGE = $th->getMessage();
		} finally {
			deliver_response($RETURN_CODE, $STATUS_MESSAGE, null);
		}
		break;
	
	default :
		deliver_response(405, "not implemented method", null);
		break;
}
