<?php

/** @file reactions.php
 * this file is the part of the rest api witch manage the reaction ressouce
 */

require_once("../utils/jwt_utils.php");
require_once("../utils/Exception.php");
require_once("../utils/utils.php");

require_once("../functions/member.php");
require_once("../functions/publication.php");


header("Content-Type:application/json");
$http_method = $_SERVER['REQUEST_METHOD'];

try {
	if (empty($_GET['id_publication'])) { throw new ExceptionMissingParameter(); }
	if (!exist_publication($_GET['id_publication'])) { throw new ExceptionRessourceNotFound(); }
	$id_publication = trim(htmlentities($_GET['id_publication']));

	$bearer_token = get_bearer_token();
	if ($bearer_token == null) { throw new ExceptionAccountNeeded(); }
	if (!is_jwt_valid($bearer_token, get_jwt_secret())) { throw new ExceptionInvalidJWTToken(); }
	$member = get_body_token($bearer_token);

} catch (\Throwable $th) {
	$RETURN_CODE = $th->getCode();
	$STATUS_MESSAGE = $th->getMessage();
	deliver_response($RETURN_CODE, $STATUS_MESSAGE, null);
	die();
}

switch ($http_method) {

	case "GET" :
		try {
			if (is_moderator($member)) {
				$result = get_reaction_of_publication_moderator($id_publication);
			} elseif (is_publisher($member)) {
				$result = get_reaction_of_publication_publisher($id_publication);
			}

			$DATA = $result;
			$RETURN_CODE = 200;
			$STATUS_MESSAGE = "Successfully obtained";
		} catch (\Throwable $th) {
			$RETURN_CODE = $th->getCode();
			$STATUS_MESSAGE = $th->getMessage();
			$DATA = null;
		} finally {
			deliver_response($RETURN_CODE, $STATUS_MESSAGE, $DATA);
		}
		break;

	case "POST" :
		try {
			$postedData = file_get_contents('php://input');
			$data=json_decode($postedData, true);

			if (!is_publisher($member)) { throw new ExceptionInsufficiantPermission(); }
			if (is_owner_of_publication($member['id_member'], $id_publication)) { throw new ExceptionLikeOwnPublication(); }
			if (empty($data['type'])) { throw new ExceptionMissingParameter(); }

			if (has_react($id_publication, $member['id_member'])) {
				remove_reaction_of_publication($id_publication, $member['id_member']);
			}

			if ($data['type'] === "like") {
				like_publication($id_publication, $member['id_member']);
			} elseif ($data['type'] === "dislike") {
				dislike_publication($id_publication, $member['id_member']);
			} else { throw new ExceptionInvalidParameter(); }

			$result = get_reaction_of_publication_publisher($id_publication);

			$DATA = $result;
			$RETURN_CODE = 201;
			$STATUS_MESSAGE = "Successfully inserted";
		} catch (\Throwable $th) {
			$RETURN_CODE = $th->getCode();
			$STATUS_MESSAGE = $th->getMessage();
			$DATA = null;
		} finally {
			deliver_response($RETURN_CODE, $STATUS_MESSAGE, $DATA);
		}
		break;

	case "DELETE" :
		try {
			if (!is_publisher($member)) { throw new ExceptionInsufficiantPermission(); }

			remove_reaction_of_publication($id_publication, $member['id_member']);

			$RETURN_CODE = 200;
			$STATUS_MESSAGE = "Successfully deleted";
		} catch (\Throwable $th) {
			$RETURN_CODE = $th->getCode();
			$STATUS_MESSAGE = $th->getMessage();
		} finally {
			deliver_response($RETURN_CODE, $STATUS_MESSAGE, null);
		}
		break;
	
	default :
		deliver_response(405, "not implemented method", null);
		break;
}
