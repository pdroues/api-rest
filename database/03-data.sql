Use apiD;

-- Users for connection
INSERT INTO `member`(`id_member`, `pseudo`, `password`, `role`) VALUES
(1, "MODERATOR", "$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma", "moderator"),
(2, "PUBLISHER", "$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma", "publisher");

-- Data for tests
INSERT INTO `member`(`id_member`, `pseudo`, `password`, `role`) VALUES
(3, "userModerator1", "$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma", "moderator"),
(4, "userPubulisher1", "$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma", "publisher"),
(5, "userPubulisher2", "$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma", "publisher");

INSERT INTO `publication`(`id_publication`, `content`, `date_publication`, `id_member`) VALUES
(1, "ceci est une publication 1", CURRENT_DATE, 4),
(2, "ceci est une publication 2", CURRENT_DATE, 5),
(3, "ceci est une publication 3", CURRENT_DATE, 4),
(4, "je vais me faire supprimer par moderator", CURRENT_DATE, 5);

INSERT INTO `reaction`(`id_publication`, `id_member`, `type`) VALUES
(1, 4, 1),
(1, 5, 0),
(2, 4, 0);
