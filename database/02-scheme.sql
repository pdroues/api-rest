USE apiD;

CREATE TABLE member(
	`id_member` INT AUTO_INCREMENT,
	`pseudo` VARCHAR(50) NOT NULL,
	`password` CHAR(61) NOT NULL,
	`role` VARCHAR(20) NOT NULL CHECK(`role` IN ("moderator", "publisher")),
	PRIMARY KEY(`id_member`),
	UNIQUE(`pseudo`)
);

CREATE TABLE publication(
	`id_publication` INT AUTO_INCREMENT,
	`content` TEXT NOT NULL,
	`date_publication` DATETIME NOT NULL,
	`id_member` INT NOT NULL,
	PRIMARY KEY(`id_publication`),
	FOREIGN KEY(`id_member`) REFERENCES `member`(`id_member`)
);

CREATE TABLE reaction(
	`id_publication` INT,
	`id_member` INT,
	`type` BOOLEAN NOT NULL,
	PRIMARY KEY(`id_publication`, `id_member`),
	FOREIGN KEY(`id_publication`) REFERENCES `publication`(`id_publication`) ON DELETE CASCADE,
	FOREIGN KEY(`id_member`) REFERENCES `member`(`id_member`) ON DELETE CASCADE
);
