<?php

/** @file Exception.php
 * this file contains all the classes related to the exceptions
 */

/**
 * @class MyException
 * @brief This is the mother class of all personalized exception
 */
class MyException extends Exception
{
	/**
	 * @brief constructor who call is mother constructor with 2 param given from child classes
	 * @param string $error the error message
	 * @param int $error_code the error code 
	 */
	public function __construct(string $error, int $error_code)
	{
		parent::__construct($error, $error_code);
	}
}

/**
 * @class ExceptionRessourceNotFound
 * @brief This exception is raise when the item call does not exist in database
 */
class ExceptionRessourceNotFound extends MyException
{
	/**
	 * @brief constructor who call his mother constructor's with static parameters
	 */
	public function __construct()
	{
		parent::__construct("Ressources not found", 404);
	}
}

/**
 * @class ExceptionDatabase
 * @brief This exception is raise when the connection with database is impossible
 */
class ExceptionDatabase extends MyException
{
	/**
	 * @brief constructor who call his mother constructor's with static parameters
	 */
	public function __construct()
	{
		parent::__construct("Error on database", 500);
	}
}

/**
 * @class ExceptionMissingParameter
 * @brief This exception is raise when somewone who want to create or modify an item forgot one or more parameter
 */
class ExceptionMissingParameter extends MyException
{
	/**
	 * @brief constructor who call his mother constructor's with static parameters
	 */
	public function __construct()
	{
		parent::__construct("missing parameters", 400);
	}
}

/**
 * @class ExceptionInvalidCredential
 * @brief This exception is raise when the combo pseudo/password does not match
 */
class ExceptionInvalidCredential extends MyException
{
	/**
	 * @brief constructor who call his mother constructor's with static parameters
	 */
	public function __construct()
	{
		parent::__construct("invalid credential", 400);
	}
}

/**
 * @class ExceptionInsufficiantPermission
 * @brief This exception is raise when somewone whant to do something and this is forbiden
 */
class ExceptionInsufficiantPermission extends MyException
{
	/**
	 * @brief constructor who call his mother constructor's with static parameters
	 */
	public function __construct()
	{
		parent::__construct("Insuficiant permission", 403);
	}
}

/**
 * @class ExceptionAccountNeeded
 * @brief This exception is raise when the action caller need to be connected to execute that
 */
class ExceptionAccountNeeded extends MyException
{
	/**
	 * @brief constructor who call his mother constructor's with static parameters
	 */
	public function __construct()
	{
		parent::__construct("Need to be connected", 401);
	}
}

/**
 * @class ExceptionInvalidJWTToken
 * @brief This exception is raise when the JWT token is invalid
 */
class ExceptionInvalidJWTToken extends MyException
{
	/**
	 * @brief constructor who call his mother constructor's with static parameters
	 */
	public function __construct()
	{
		parent::__construct("Invalid JWT token", 400);
	}
}

/**
 * @class ExceptionInvalidParameter
 * @brief This exception is raise when the parameter of the function are incorrect
 */
class ExceptionInvalidParameter extends MyException
{
	/**
	 * @brief constructor who call his mother constructor's with static parameters
	 */
	public function __construct()
	{
		parent::__construct("Invalid parameters", 400);
	}
}

/**
 * @class ExceptionLikeOwnPublication
 * @brief This exception is raise when a publisher try to react at his own publication
 */
class ExceptionLikeOwnPublication extends MyException
{
	/**
	 * @brief constructor who call his mother constructor's with static parameters
	 */
	public function __construct()
	{
		parent::__construct("Can not self react to our publication", 400);
	}
}
