<?php

/** @file srv-token.php
 * this file is the api who deliver a jwt token
 */

require_once("../utils/jwt_utils.php");
require_once("../utils/utils.php");

require_once("../functions/member.php");

header("Content-Type:application/json");
$http_method = $_SERVER['REQUEST_METHOD'];

switch ($http_method) {

	case "POST" :
		try {
			$postedData = file_get_contents('php://input');
			$data=json_decode($postedData, true);

			$user = connect($data['pseudo'], $data['password']);

			$header = array("alg" => "HS256", "typ" => "JWT");
			$payload = array("id_member" => $user['id_member'], "pseudo" => $user['pseudo'], "role" => $user['role'], "exp" => (time()+3600));

			$DATA = generate_jwt($header, $payload, get_jwt_secret());
			$RETURN_CODE = 201;
			$STATUS_MESSAGE = "connection ok";
		} catch (\Throwable $th) {
			$RETURN_CODE = $th->getCode();
			$STATUS_MESSAGE = $th->getMessage();
			$DATA = null;
		} finally {
			deliver_response($RETURN_CODE, $STATUS_MESSAGE, $DATA);
		}
		break;
	
	default :
		deliver_response(405, "not implemented method", null);
		break;
}
