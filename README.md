# Identifiants

moderator password
publisher password

# Specification api metier

## publications

### URI

```
{GET, POST} X/api/publications
{GET, PUT, DELETE} X/api/publications/id_publication
```

### Role: 'moderator'

#### Methode

- GET
- DELETE

#### Exemple de retour

```json
{
    "auteur": "pseudo",
    "date_publication":"2010/12/27",
    "contenu":"long string",
    "nbr_like":3,
    "nbr_dislike":4,
    "like":[ "pseudo", "pseudo" ],
    "dislike": [ "pseudo", "pseudo" ]
}
```

### Role: 'publisher'

#### Methode

##### Sur une publication lui appartenant

- GET
- POST
- PUT
- DELETE

##### Sur n'importe quel publication

- GET

#### Exemple d'appel

##### POST

```json
{
    "contenu":"long string"
}
```

##### PUT

```json
{
    "id_publication":3,
    "contenu":"long string"
}
```

#### Exemple de retour

```json
{
    "auteur": "pseudo",
    "date_publication":"2010/12/27",
    "contenu":"long string",
    "nbr_like":3,
    "nbr_dislike":4
}
```

### Role: 'non conected user'

#### Methode

- GET

#### Exemple de retour

```json
{
    "auteur": "pseudo",
    "date_publication":"2010/12/27",
    "contenu":"long string"
}
```

## reactions

### URI

```
{GET, POST, DELETE} X/api/publications/id_publication/reactions
```

### Role: 'moderator'

#### Methode

- GET

#### Exemple de retour

```json
{
    "nbr_like":3,
    "nbr_dislike":4,
    "like":[ "pseudo", "pseudo" ],
    "dislike": [ "pseudo", "pseudo" ]
}
```

### Role: 'publisher'

#### Methode

- GET
- POST
- DELETE

#### Exemple d'appel

##### POST

```json
{
    "type": "like" // or "dislike"
}
```

#### Exemple de retour

```json
{
    "nbr_like":3,
    "nbr_dislike":4
}
```

### Role: 'non conected user'

#### Methode

Aucune

# Specification api connection

## Donnée attendu (metode POST)

```json
{
    "pseudo":"pseudo",
    "password":"password"
}
```

## Exemple de retour

```json
{
    "id_member":3,
    "pseudo":"pseudo",
    "roles":[ "moderator", "publisher" ],
    "exp": 255
}
```